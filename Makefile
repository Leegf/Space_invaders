.PHONY: all clean fclean re

NAME = ft_retro

CC = clang++

OBJ =	main.o         		\
		player.o         		\
		Game.o 				\
		Window.o 				\
		bullet.o 				\
		Foe.o 				\
		Score.o 				\



CPPFLAGS = -Wall -Wextra -Werror #-lncurses

all: $(NAME) 

$(NAME): $(OBJ) 
	$(CC) $(CPPFLAGS) $(OBJ) -lncurses -o $(NAME) 

$(OBJ):%.o: %.cpp
		$(CC) $(CPPFLAGS) -c $< -o $@
clean:
	rm -f $(OBJ)

fclean: clean
		rm -f $(NAME)

re: fclean all 
