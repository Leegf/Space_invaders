Here's a simple arcade game made in haste(two days without prior knowledge of ncurses library).

Controls: `arrows` to move around and `space` to shoot. You can win by scoring 300 points or die if hp gets below 0.

You may compile it via `Makefile` or using CmakeLists.txt. Also, you have ncurses to be installed. 